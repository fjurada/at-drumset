

import pygame.mixer

from time import sleep

import RPi.GPIO as GPIO

from sys import exit


GPIO.setmode(GPIO.BCM)

GPIO.setup(17, GPIO.IN)
GPIO.setup(22, GPIO.IN)
GPIO.setup(23, GPIO.IN)
GPIO.setup(24, GPIO.IN)
GPIO.setup(25, GPIO.IN)
GPIO.setup(30, GPIO.IN)


pygame.mixer.init(48000, -16, 2, 512)


sndA = pygame.mixer.Sound("tom1.wav")
sndB = pygame.mixer.Sound("hhcl.wav")
sndC = pygame.mixer.Sound("heavyk1.wav")
sndD = pygame.mixer.Sound("crash.wav")
sndE = pygame.mixer.Sound("snare1.wav")
sndF = pygame.mixer.Sound("bd.wav")


soundChannelA = pygame.mixer.Channel(1)
soundChannelB = pygame.mixer.Channel(2)
soundChannelC = pygame.mixer.Channel(3)
soundChannelD = pygame.mixer.Channel(4)
soundChannelE = pygame.mixer.Channel(5)
soundChannelF = pygame.mixer.Channel(6)


print "Soundboard Ready."



while True:

	try:

		if (GPIO.input(17) == False):
			soundChannelA.play(sndA)

		if (GPIO.input(22) == False):
			soundChannelB.play(sndB)

		if (GPIO.input(23) == False):
			soundChannelC.play(sndC)

		if (GPIO.input(24) == False):
			soundChannelD.play(sndD)

		if (GPIO.input(25) == False):
			soundChannelE.play(sndE)

		if (GPIO.input(30) == False):
			soundChannelF.play(sndF)

		sleep(0.2)

	except KeyboardInterrupt:

		exit()