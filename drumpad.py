

import pygame.mixer

from time import sleep

import RPi.GPIO as GPIO

from sys import exit


GPIO.setmode(GPIO.BCM)

GPIO.setup(17, GPIO.IN)
GPIO.setup(22, GPIO.IN)
GPIO.setup(23, GPIO.IN)
GPIO.setup(24, GPIO.IN)
GPIO.setup(25, GPIO.IN)
GPIO.setup(30, GPIO.IN)


pygame.mixer.init(48000, -16, 2, 512)


sndA = pygame.mixer.Sound("tom1.wav")
sndB = pygame.mixer.Sound("hhcl.wav")
sndC = pygame.mixer.Sound("heavyk1.wav")
sndD = pygame.mixer.Sound("crash.wav")
sndE = pygame.mixer.Sound("snare1.wav")
sndF = pygame.mixer.Sound("bd.wav")


soundChannelA = pygame.mixer.Channel(1)
soundChannelB = pygame.mixer.Channel(2)
soundChannelC = pygame.mixer.Channel(3)
soundChannelD = pygame.mixer.Channel(4)
soundChannelE = pygame.mixer.Channel(5)
soundChannelF = pygame.mixer.Channel(6)


ch1Down = False;
ch2Down = False;
ch3Down = False;
ch4Down = False;
ch5Down = False;
ch6Down = False;


print "Soundboard Ready."



while True:

	try:

		if (GPIO.input(17) == False):
			if (ch1Down == False):

				soundChannelA.play(sndA)
				ch1Down = True
		else:

			ch1Down = False

		if (GPIO.input(22) == False):
			if (ch2Down == False):
				soundChannelB.play(sndB)
				ch2Down = True
		else:
			ch2Down = False

		if (GPIO.input(23) == False):
			if (ch3Down == False):
				soundChannelC.play(sndC)
				ch3Down = True
		else:
			ch3Down = False

		if (GPIO.input(24) == False):
			if (ch4Down == False):
				soundChannelD.play(sndD)
				ch4Down = True
		else:
			ch4Down = False

		if (GPIO.input(25) == False):
			if (ch5Down == False):
				soundChannelE.play(sndE)
				ch5Down = True
		else:
			ch5Down = False

		if (GPIO.input(30) == False):
			if (ch6Down == False):
				soundChannelF.play(sndF)
				ch6Down = True
		else:
			ch6Down = False

		sleep(0.01)

	except KeyboardInterrupt:

		exit()
